package com.captton.demoapi.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.demoapi.model.Saludo;

@RestController
//@RequestMapping({"/consultas"})
public class MainController {
	
	@GetMapping
	public ResponseEntity<Object> saludar() {
		
		JSONObject obj = new JSONObject();
		
		obj.put("error", 0);
		obj.put("saludo", "hola chicos!!!");
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@GetMapping({"/mejorsaludo"})
	public String saludarMejor() {
		
		return "hola mundo, que bello dia";
	}
	
	@GetMapping("/mejorsaludo2/{nom}")
	public String saludarMejor2(@PathVariable String nom) {
		
		return "hola, buen dia "+nom;
	}
	
	@PostMapping({"/guardar"})
	public ResponseEntity<Object> guardarSaludo(@RequestBody Saludo saludo, 
			@RequestHeader(name = "Authorization", required = true) String headerAuth,
			@RequestHeader(name = "Otro", required = true) String headerOtro) {
		
		System.out.println("remitente: "+saludo.getRemitente());
		System.out.println("destinatario: "+saludo.getDestinatario());
		System.out.println("mensaje: "+saludo.getMensaje());
		
		System.out.println("Auth y el Otro: "+headerAuth +" / "+headerOtro);
		
		if(saludo.getDestinatario().contentEquals("Jose")) {
			
			JSONObject obj = new JSONObject();
			
			obj.put("message", "su mensaje ha sido guardado");
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("message", "solo guardamos mensajes para Jose");
			
			
		//	return ResponseEntity.status(HttpStatus.NOT_FOUND).body(obj.toString());
			return ResponseEntity.ok().body(obj.toString());
			
			
		}
		
	//	return ResponseEntity.ok().body(saludo);
	}
	
	

}
